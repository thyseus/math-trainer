package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
)

type skill struct {
	skill     int
	label     string
	retries   int
	minNumber int
	maxNumber int
}

func main() {
	var choosenSkillInt int
	var choosenSkill skill

	var skills = []skill{
		{
			skill:     1,
			label:     "very easy",
			retries:   10,
			minNumber: 1,
			maxNumber: 10,
		}, {
			skill:     2,
			label:     "easy",
			retries:   5,
			minNumber: 10,
			maxNumber: 100,
		}, {
			skill:     3,
			label:     "normal",
			retries:   3,
			minNumber: 100,
			maxNumber: 1000,
		}, {
			skill:     4,
			label:     "hard",
			retries:   2,
			minNumber: 1000,
			maxNumber: 10000,
		}, {
			skill:     5,
			label:     "very hard",
			retries:   1,
			minNumber: 10000,
			maxNumber: 100000,
		},
	}

	for _, skill := range skills {
		fmt.Printf("Skill %d) (%s):\n", skill.skill, skill.label)
		fmt.Printf("Retries: %d, ", skill.retries)
		fmt.Printf("Min Number: %d, ", skill.minNumber)
		fmt.Printf("Max Number: %d.\n", skill.maxNumber)
	}

	fmt.Printf("\nPlease choose a skill (1-5) -> ")
	fmt.Scanln(&choosenSkillInt)

	choosenSkill = skills[choosenSkillInt-1]

	fmt.Printf("You have selected skill %s.\n", choosenSkill.label)

	var seed int64
	fmt.Printf("Please enter a seed for the random number generator (integer) -> ")
	fmt.Scanln(&seed)

	rand := rand.New(rand.NewSource(seed))

	fmt.Printf("Hint: Use the number 0 to exit the Program.\n")

	for {
		var number1 int
		var number2 int
		var response string
		var responseInt int

		number1 = rand.Intn(choosenSkill.maxNumber) + 1
		number2 = rand.Intn(choosenSkill.maxNumber) + 1

		fmt.Printf("%d + %d = ", number1, number2)
		fmt.Scanln(&response)

		responseInt, _ = strconv.Atoi(response)

		fmt.Printf("You entered: %s.\n", response)

		if response == "0" {
			fmt.Println("Exiting the program. Goodbye!")
			os.Exit(0)
		}

		if number1+number2 == responseInt {
			fmt.Println("That is correct.")
		} else {
			fmt.Println("That is wrong.")
		}

	}
}
