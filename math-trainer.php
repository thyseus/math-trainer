<?php

echo "This is a program to learn basic mathematics.\n";

echo "Minimum number to ask for ? [0]:";

$min = readline();

if (! $min) {
    $min = 0;
}

echo "Maximum number to ask for ? [10]:";

$max = readline();

if (! $max) {
    $max = 10;
}

if ($min == $max) {
    echo "Min and Max should not be equal. Exiting.\n";
    exit(0);
}

echo "How many rounds do you want to play? [10]:";

$rounds = readline();

if (! $rounds) {
    $rounds = 10;
}

$points = 0;

for ($i = 1; $i <= $rounds; $i++) {
    $a = rand($min, $max);
    $b = rand($min, $max);

    $operators = ["+", "-", "*", "/"];

    $c = rand(0, count($operators) - 1);
    $operator = $operators[$c];

    // ensure good quality of division numbers:
    if ($operator == '/') {
        $j = 0;
        // attempt 10 times to find a good division (avoid infinite loop if there is no possible combination)
        while ($j <= 10
            && $b != 0  // avoid division by zero
            && ($a % $b != 0 || $a == $b || $b == 1)
        ) {
            $a = rand($min, $max);
            $j++;
        }

        if ($j >= 10) {
            $operator = $operators[0];
        }
    }

    echo "-----------------------------------------------------\n";
    echo "Current score: $points, Round $i:                $a $operator $b = ?\n";

    $input = readline();

    echo "You have entered: $input\n";

    if ($operator == "+") {
        $result = $a + $b;
    }

    if ($operator == "-") {
        $result = $a - $b;
    }

    if ($operator == "*") {
        $result = $a * $b;
    }

    if ($operator == "/") {
        $result = $a / $b;
    }

    if ($result == $input) {
        echo "This is correct. You have gained one point.\n";
        $points++;
    } else {
        echo "This is wrong. The correct result would be: $result\n";
    }
}

echo "You have achieved $points out of $rounds points.\n";
